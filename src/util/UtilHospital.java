package util;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import hospitalAgain.Animal;
import hospitalAgain.History;
import hospitalAgain.Owner;
import hospitalAgain.Personalmedical;
import hospitalAgain.Programare;
public class UtilHospital {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp(){
		entityManagerFactory = Persistence.createEntityManagerFactory("hospitalAgain");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);		
	}
	public void saveOwner(Owner owner) {
		entityManager.persist(owner);
	}
	public void saveHistory(History history) {
		entityManager.persist(history);
	}
	public void savePersonalMedical(Personalmedical personalMedical) {
		entityManager.persist(personalMedical);
	}
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager() {
		entityManager.close();
	}
	public void stop() {
		entityManager.close();
	}
	public void printAllAnimalsFromDB() {
		@SuppressWarnings("unchecked")
		List<Animal> results = entityManager.createNativeQuery("Select * fromhospital.Animal",Animal.class).getResultList();
		for(Animal animal : results) {
			System.out.println("Animal: " + animal.getName() + " hasID " + animal.getIdAnimal() + " hasColor: " + animal.getColor() + " hasBirthYear " + animal.getBirth() );
		}
	}
	
	//CRUD Animal
	public void createAnimal(int idAnimal, int bornYear ,String color ,String name) {
		entityManager.getTransaction().begin();
		entityManager.persist(new Animal(idAnimal,bornYear,color,name));
		entityManager.getTransaction().commit();
	}
	public void readAnimal(int idAnimal) {
		Animal obj = entityManager.find(Animal.class, idAnimal);
		obj.printObject();
	}
	public void updateAnimal(int idAnimal,int bornYear ,String color ,String name) {
		Animal obj = entityManager.find(Animal.class, idAnimal);
		entityManager.getTransaction().begin();
		obj.setBirth(bornYear);
		obj.setColor(color);
		obj.setName(name);
		entityManager.getTransaction().commit();
	}
	public void deleteAnimal(int idAnimal) {
		Animal obj = entityManager.find(Animal.class, idAnimal);
		entityManager.getTransaction().begin();
		entityManager.remove(obj);
		entityManager.getTransaction().commit();
	}
	
	//CRUD PersonalMediacal
	public void createPersonalMedical(int idPersonalMedical, String name,int salary) {
		entityManager.getTransaction().begin();
		entityManager.persist(new Personalmedical(idPersonalMedical,name,salary));
		entityManager.getTransaction().commit();
	}	
	public void readPersonalMedical(int idPersonalMedical) {
		
		Personalmedical obj = entityManager.find(Personalmedical.class, idPersonalMedical);
		System.out.println(obj.getIdpersonalMedical() + " " + obj.getName() + " "+ obj.getSalary() );
	}
	public void updatePersonalMedical(int idPersonalMedical, String name,  int salary) {
		Personalmedical obj = entityManager.find(Personalmedical.class, idPersonalMedical);
		entityManager.getTransaction().begin();
		obj.setIdpersonalMedical(idPersonalMedical);
		obj.setName(name);
		obj.setSalary(salary);
		entityManager.getTransaction().commit();
	}
	public void deletePersonalMedical(int idPersonalMedical) {
		Personalmedical obj = entityManager.find(Personalmedical.class, idPersonalMedical);
		entityManager.getTransaction().begin();
		entityManager.remove(obj);
		entityManager.getTransaction().commit();
	}
	
	//CRUD Programare
	public void createProgramare(int idProgramare, Date appoiment) {
		entityManager.getTransaction().begin();
		entityManager.persist(new Programare(idProgramare,appoiment));
		entityManager.getTransaction().commit();
	}
	public void readProgramare(int idProgramare) {
		Programare obj = entityManager.find(Programare.class,idProgramare );
		System.out.println(obj.getIdProgramare() + " " + obj.getDataProgramare());
	}
	public void updateProgramare(int idProgramare, Date appoiment) {
		Programare obj = entityManager.find(Programare.class, idProgramare);
		entityManager.getTransaction().begin();
		obj.setIdProgramare(idProgramare);
		obj.setDataProgramare(appoiment);
		entityManager.getTransaction().commit();
	}
	public void deleteProgramare(int idPersonalMedical) {
		Programare obj = entityManager.find(Programare.class, idPersonalMedical);
		entityManager.getTransaction().begin();
		entityManager.remove(obj);
		entityManager.getTransaction().commit();
	}
	
	//CRUD Owner
	public void createOwner(int idOwner, String nameOwner) {
		entityManager.getTransaction().begin();
		entityManager.persist(new Owner(idOwner,nameOwner));
		entityManager.getTransaction().commit();
	}
	public void readOwner(int idOwner, String nameOwner) {
		Owner obj = entityManager.find(Owner.class, idOwner);
		System.out.println(obj.getIdOwner() + " " + obj.getNameOwner());
	}
	public void updateOwner(int idOwner, String nameOwner) {
		Owner obj = entityManager.find(Owner.class, idOwner);
		entityManager.getTransaction().begin();
		obj.setIdOwner(idOwner);
		obj.setNameOwner(nameOwner);
		entityManager.getTransaction().commit();
	}
	public void deleteOwner(int idOwner) {
		Owner obj = entityManager.find(Owner.class, idOwner);
		entityManager.getTransaction().begin();
		entityManager.remove(obj);
		entityManager.getTransaction().commit();
	}

	//CRUD History
	public void createHistory(int idHistory, String medicalEvents, String treatment) {
		entityManager.getTransaction().begin();
		entityManager.persist(new History(idHistory,medicalEvents,treatment));
		entityManager.getTransaction().commit();
	}
	public void readHistory(int idHistory, String medicalEvents, String treatment) {
		History obj = entityManager.find(History.class, idHistory);
		System.out.println(obj.getIdHistory() + " " + obj.getMedicalEvents() + " " + obj.getTreatment() );
	}
	public void updateHistory(int idHistory, String medicalEvents, String treatment) {
		History obj = entityManager.find(History.class, idHistory);
		entityManager.getTransaction().begin();
		obj.setIdHistory(idHistory);
		obj.setMedicalEvents(medicalEvents);
		obj.setTreatment(treatment);
		entityManager.getTransaction().commit();
	}
	public void deleteHistory(int idHistory) {
		History obj = entityManager.find(History.class, idHistory);
		entityManager.getTransaction().begin();
		entityManager.remove(obj);
		entityManager.getTransaction().commit();
	}
	@SuppressWarnings("unchecked")
	public List<Animal> animalList() {
		return entityManager.createNativeQuery("Select * from hospital.animal", Animal.class).getResultList();	
	}

}
