package controller;
import java.awt.Color;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import hospitalAgain.Animal;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import util.UtilHospital;

public class MainController implements Initializable {

	@FXML
	private ListView<String>listView;
	private ScrollPane scrollPane;
	public void populateMainListView() {
		UtilHospital utilDb = new UtilHospital();
		utilDb.setUp();
		utilDb.startTransaction();
		List<Animal>animalDbList = (List<Animal>) utilDb.animalList();
		ObservableList<String> animalNamesList = getAnimalName(animalDbList);
		//listView.setItems(animalNamesList);
		//listView.refresh();
		//ObservableList<String> data = FXCollections.observableArrayList("x","y","z");
		//listView.setItems(data);
		//listView.getItems().addAll("x","y","z");
		utilDb.stop(); 
	}
	public ObservableList<String>getAnimalName (List<Animal>animals){
		ObservableList<String> names = FXCollections.observableArrayList();
		for(Animal a: animals) {
			names.add(a.getName());
		}
		return names;
	}
	public void populateScrollPane() {
		scrollPane.applyCss();
		//Background b = new Background();
		//scrollPane.backgroundProperty();
		//scrollPane.setBorder(value);
	}
	@Override
	public void initialize(URL location, ResourceBundle resources){
		populateMainListView();
		
	}

}
